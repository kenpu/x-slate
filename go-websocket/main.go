package main

import (
	"code.google.com/p/go.net/websocket"
	"log"
	"net/http"
	"time"
)

type Message struct {
	Name string `json:"name"`
	Body string `json:"body"`
}

var (
	incoming = make(chan string)
	outgoing = make(chan string)
	done     = make(chan bool)
)

func Reader(ws *websocket.Conn) {
	for {
		var data Message
		websocket.JSON.Receive(ws, &data)
		log.Printf("reading... %#v\n", data)
	}
}

func Writer(ws *websocket.Conn) {
	var data = "Hello World"
	for {
		<-time.Tick(1 * time.Second)
		log.Println("writing...")
		websocket.JSON.Send(ws, data)
	}
}

func wsHandler(ws *websocket.Conn) {
	log.Println("wsHandler...")
	go Writer(ws)
	Reader(ws)
	<-done
}

func main() {
	http.Handle("/ws", websocket.Handler(wsHandler))
	http.Handle("/", http.FileServer(http.Dir(".")))
	log.Fatal(http.ListenAndServe(":8000", nil))
}
