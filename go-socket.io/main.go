package main

import (
	"github.com/googollee/go-socket.io"
	"log"
	"net/http"
)

func main() {
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}

	http.Handle("/rt/", server)
	http.Handle("/", http.FileServer(http.Dir(".")))
	log.Println("Go Socket.IO server")
	log.Fatal(http.ListenAndServe(":5000", nil))
}
